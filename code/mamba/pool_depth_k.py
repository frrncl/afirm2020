# Copyright 2018-2020 University of Padua, Italy
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Author: Nicola Ferro (ferro@dei.unipd.it)
# Author: Maria Maistro (mm@di.ku.dk)


def pool_depth_k(imported_runs, k = 50):
	"""Computes the pool at the given depth value.

	Args:
		imported_runs: list of imported runs.
		k: the pool depth.
		   The default is 10.

	Returns:
		The pool of documents for the given runs.
	"""

	# Initialize the set of topic ids
	topic_ids = set()

	# Extract the topic ids from the first run
	# For each tuple in the run (topic_id, doc_id, score)
	for tup in imported_runs[0]:

		# Get the first element of the tuple, i.e. the topic id
		# and add it to the set of topic ids
		topic_ids.add(tup[0])

	# Initialize the pool as a dictionary
	# Keys are topic ids
	# Values are set of documents ids, i.e. the documents in the pool
	pool = {x: set() for x in topic_ids}

	# For each of the imported runs
	for current_run in imported_runs:

		# Get the first topic id
		previous_topic_id = current_run[0][0]
		# Initialize a counter for the document rank position
		idx = 0

		# For each tuple in the run
		for tup in current_run:

			# Get the current topic id and document id
			current_topic_id = tup[0]
			current_doc_id = tup[1]

			# If the topic has NOT changed
			if previous_topic_id == current_topic_id and idx < k:

				# Add the current document to the pool
				pool[current_topic_id].add(current_doc_id)

				# Update rank position index
				idx = idx + 1

			# If the topic has changed
			elif previous_topic_id != current_topic_id:

				# Add the current document to the pool
				pool[current_topic_id].add(current_doc_id)

				# Update the previous topic id
				previous_topic_id = current_topic_id

				# Update rank position index (the first document has already been added)
				idx = 1

	return pool
