# Information Retrieval Evaluation at AFIRM 2020

This repository contains slides, source code, and additional material complementing the lectures:

* _The Role of Evaluation for Search_ by Nicola Ferro and Maria Maistro
* _The Role of Evaluation for Search - Lab_ by Nicola Ferro and Maria Maistro

held at the [ACM SIGIR/SIGKDD Africa Summer School on Machine Learning for Data Mining and Search](http://sigir.org/afirm2020/) (AFIRM 2020), 27-31 January 2020, Cape Town, South Africa.

Copyright and license information can be found in the file LICENSE. 
Additional information can be found in the file NOTICE.


The repository also contains a `trec_eval.9.0` folder where the `trec_eval` source code is stored. 
The code is copied from [https://trec.nist.gov/trec_eval/](https://trec.nist.gov/trec_eval/). 
Note that the file `trec_eval.c` has been modified (thanks to Hussein Suleman) to fix a segmentation fault problem due int/long issues on 64-bit architectures. The original `trec_eval.c` files has been renamed `trec_eval.c.orig`.

